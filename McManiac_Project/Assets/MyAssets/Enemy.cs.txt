﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    public float HP = 30;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void TakeDamage(float damage)
    {
        HP -= damage;
        if (HP <= 0)
            Destroy(this.gameObject, 0f);
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Bullet"){
            Debug.Log(col.relativeVelocity.magnitude);
            TakeDamage(col.relativeVelocity.magnitude);
            Destroy(col.gameObject);
        }
    }
  }
