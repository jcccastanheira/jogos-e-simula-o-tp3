﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    public float HP = 30;
    public float minDamage = 3;
    public float chillAfterHit = 1;
    public Transform Player;
    public float MoveSpeed = 4;
    public float MaxDist = 10;
    public float MinDist = 5;
    public GameObject impact;
	public Rigidbody rb;
	public Animator anim;
    private MeshRenderer m;
    private bool isAttacking;
    float lastHit;
    public new AudioSource audio;

    // Use this for initialization
    void Start () {
        transform.position = new Vector3(transform.position.x, 0, transform.position.z);
        m = this.gameObject.GetComponent<MeshRenderer>();

    }

    // Update is called once per frame
    void Update()
    {
        Player = GameObject.FindGameObjectWithTag("Player").transform;
        transform.LookAt(Player);

        if (Time.time - lastHit >= chillAfterHit) {
			if (Vector3.Distance(transform.position, Player.position) >= MinDist && Vector3.Distance(transform.position, Player.position) <= MaxDist)
            {
				
				rb.velocity = transform.forward * MoveSpeed;
				isAttacking = true;
				anim.SetBool("isAttacking",true);
			}        
			else
			{
				rb.velocity = Vector3.zero;
				anim.SetBool("isAttacking",false);
				isAttacking = false;
			}
        }
        else
        {
			rb.velocity = Vector3.zero;
			isAttacking = false;
			anim.SetBool("isAttacking",false);	
        }
    }

    void TakeDamage(float damage)
    {
        HP -= damage;
        if (HP <= 0)
        {
            m.enabled = false;
            Destroy(this.gameObject, 0.7f);
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Bullet")
        {
            if (col.relativeVelocity.magnitude >= minDamage)
            {
                GameObject expl = Instantiate(impact, col.gameObject.transform.position, Quaternion.identity) as GameObject;
                audio.Play();
                Destroy(expl, 0.5f);
                TakeDamage(col.relativeVelocity.magnitude*2);
                col.rigidbody.velocity = Vector3.Reflect(transform.position, col.contacts[0].normal);
            }
        }
        else if (col.gameObject.tag == "Player")
        {   
            lastHit = Time.time;            
        }
    }
    }
