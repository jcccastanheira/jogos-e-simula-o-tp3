﻿using UnityEngine;
using System.Collections;

public class Shooting : MonoBehaviour
{
    public int ammo;
    public float bulletSpeed = 10;
    public float minBoost = 1f;
    public float maxBoost = 3;
    public Rigidbody bullet;

    float timePressed = 0f;
    bool isPressed = false;

    float barDisplay; //current progress
    public Vector2 pos = new Vector2(20, 40);
    public Vector2 size = new Vector2(60, 20);
    public Texture2D emptyTex;
    public Texture2D Charging;

    public new AudioSource audio;
    public new AudioSource audio2;


    void Start()
    {   
             pos = new Vector2(Screen.width/2, 0.9f * Screen.height);

      }

void OnGUI()
    {
        //draw the background:
        GUI.BeginGroup(new Rect(pos.x, pos.y, size.x, size.y));
        GUI.Box(new Rect(0, 0, size.x, size.y), emptyTex);
        //draw the filled-in part:
        GUI.BeginGroup(new Rect(0, 0, size.x * barDisplay, size.y));
        GUI.Box(new Rect(0, 0, size.x, size.y), Charging);
        GUI.EndGroup();
        GUI.EndGroup();

        GUI.BeginGroup(new Rect(pos.x, pos.y, size.x, size.y));
        GUI.Label(new Rect(size.x/2.5f, 0, 100, 20), ammo.ToString());
        GUI.EndGroup();



    }


    void Update()
    {

        keyPressedTimer();

    }

    void keyPressedTimer()
    {
        if (isPressed)
            barDisplay = (Time.time - timePressed)/maxBoost;

        if (Input.GetKeyDown(KeyCode.Mouse0) && ammo > 0)
        {
            timePressed = Time.time;
            isPressed = true;
            audio.Play();
            
        }

        if (Input.GetKeyUp(KeyCode.Mouse0) && ammo > 0)
        {
            timePressed = Time.time - timePressed;
            isPressed = false;
            if (timePressed < minBoost / 2)
            {
                barDisplay = 0;
                return;
            }
            else {
                if (timePressed < minBoost)
                    timePressed = minBoost;
                if (timePressed > maxBoost)
                    timePressed = maxBoost;
                Fire(timePressed);
                audio2.Play();
            }
        }
    }


    void Fire(float timePressed)
    {
        Rigidbody bulletClone = (Rigidbody)Instantiate(bullet, transform.position, transform.rotation);
        bulletClone.velocity = transform.forward * bulletSpeed * timePressed;
        ammo -= 1;
        barDisplay = 0;
    }

   public void incrementAmmo()
    {
        ammo++;
    }


}


 