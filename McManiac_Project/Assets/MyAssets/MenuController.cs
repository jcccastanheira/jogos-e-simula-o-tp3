﻿using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour {

    public Canvas menu;
    public Button playButton;
    public Button exitButton;

    // Use this for initialization
	void Start () {
        menu = menu.GetComponent<Canvas>();
        playButton = playButton.GetComponent<Button>();
        exitButton = exitButton.GetComponent<Button>();
        menu.enabled = true;
        playButton.enabled = true;
        exitButton.enabled = true;
    }

    // Update is called once per frame
    void Update () {
        
	}

    public void StartLevel()
    {
        Debug.Log("lol");
        //SceneManager.LoadScene("game");
    }

    public void Exit()
    {
        Application.Quit();
    }
}
