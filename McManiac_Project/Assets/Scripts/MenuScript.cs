﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class MenuScript : MonoBehaviour {
    
    public Button startText;


	// Use this for initialization
	void Start ()
    {
        startText = startText.GetComponent<Button>();
	}

    public void startLevel()
    {
        SceneManager.LoadScene("game");
    }

    public void startLevel1()
    {
        SceneManager.LoadScene("game1");
    }

    public void startLevel2()
    {
        SceneManager.LoadScene("game2");
    }
}
